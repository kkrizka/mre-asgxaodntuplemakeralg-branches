# MRE AsgxAODNTupleMakerAlg Branches

MRE demonstrating a problem with adding certain (previously unaccessed) branches. It runs on the `ASG_TEST_FILE_DATA` file and tries to save the muon's charge value that appears in the input AOD as `MuonsAuxDyn.charge`.

The observed error is the following:
```
NTupleMakerMuons                                    DEBUG Skipping branch "mu_charge" from container/variable "Muons.charge"
NTupleMakerMuons                                    ERROR No branch was created for rule: "Muons.charge -> mu_charge" and systematics: ""
NTupleMakerMuons                                    ERROR AsgAnalysisAlgorithms/Root/AsgxAODNTupleMakerAlg.cxx:397 (StatusCode CP::AsgxAODNTupleMakerAlg::setupTree()): code FAILURE: setupBranch( branchDecl, nominal )
NTupleMakerMuons                                    ERROR AsgAnalysisAlgorithms/Root/AsgxAODNTupleMakerAlg.cxx:345 (StatusCode CP::AsgxAODNTupleMakerAlg::execute()): code FAILURE: setupTree()
```