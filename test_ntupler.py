#!/usr/bin/env python

import os

from AthenaConfiguration.ComponentAccumulator import ComponentAccumulator
from AthenaConfiguration.ComponentFactory import CompFactory

# Configure input
from AthenaConfiguration.AllConfigFlags import ConfigFlags
ConfigFlags.Input.Files = [os.environ['ASG_TEST_FILE_DATA']]

from AthenaConfiguration.MainServicesConfig import MainServicesCfg
acc = MainServicesCfg(ConfigFlags)
from AthenaPoolCnvSvc.PoolReadConfig import PoolReadCfg
acc.merge(PoolReadCfg(ConfigFlags))

ConfigFlags.lock()

#####
def SystematicsSvcCfg(flags, name='SystematicsSvc', **kwargs):
    acc = ComponentAccumulator()

    kwargs.setdefault('sigmaRecommended', 1)
    acc.addService(CompFactory.CP.SystematicsSvc(name,**kwargs))

    return acc

def TreeMakerAlgCfg(flags, name='TreeMaker', **kwargs):
    acc = ComponentAccumulator()

    acc.addEventAlgo(CompFactory.CP.TreeMakerAlg(name, **kwargs))

    return acc

def TreeFillerAlgCfg(flags, name='TreeFiller', **kwargs):
    acc = ComponentAccumulator()

    acc.addEventAlgo(CompFactory.CP.TreeFillerAlg(name, **kwargs))

    return acc

def NTupleMakerPixelClustersCfg(flags, name='NTupleMakerMuons', **kwargs):
    kwargs.setdefault('Branches', [
        'Muons.pt -> mu_pt',
        'Muons.eta -> mu_eta',
        'Muons.phi -> mu_phi',
        'Muons.charge -> mu_charge',
        'Muons.ptcone20 -> mu_ptcone20'
    ])
    kwargs.setdefault('OutputLevel', 2)

    acc = ComponentAccumulator()

    acc.addEventAlgo(CompFactory.CP.AsgxAODNTupleMakerAlg(name, **kwargs))

    return acc

def testconfig(flags):
    acc = ComponentAccumulator()

    acc.merge(SystematicsSvcCfg(flags))
    acc.merge(TreeMakerAlgCfg(flags))
    acc.merge(NTupleMakerPixelClustersCfg(flags))
    acc.merge(TreeFillerAlgCfg(flags))

    return acc

acc.merge(testconfig(ConfigFlags))
#####

# finally, set up the infrastructure for writing our output
from GaudiSvc.GaudiSvcConf import THistSvc
histSvc = CompFactory.THistSvc()
histSvc.Output += ["ANALYSIS DATAFILE='output.root' OPT='RECREATE'"]
acc.addService(histSvc)

acc.printConfig(withDetails=True)

# Execute and finish
sc = acc.run(maxEvents=-1)
 
# Success should be 0
import sys
sys.exit(not sc.isSuccess())


